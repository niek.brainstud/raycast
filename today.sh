#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Today
# @raycast.mode fullOutput

# Optional parameters:
# @raycast.icon 🖊️
# @raycast.packageName jrnl.today

# Documentation:
# @raycast.description What did i do today
# @raycast.author Niek

/usr/local/bin/jrnl -on today

