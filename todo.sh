#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Todo
# @raycast.mode silent

# Optional parameters:
# @raycast.icon ✔️
# @raycast.packageName log.todo
# @raycast.argument1 { "type": "text", "placeholder": "What do you want to do", "percentEncoded": true}

# Documentation:
# @raycast.description Log what your doing
# @raycast.author Niek
# open "obsidian://advanced-uri?vault=Notes&commandid=quickadd%253Achoice%253Ada0bf2cf-f1b7-4001-b78d-e021159bf134"
message=`echo $1 | python3 -c "import sys; from urllib.parse import unquote; print(unquote(sys.stdin.read()));"`
message="- [ ] $message"
open "obsidian://advanced-uri?vault=notes&daily=true&data=$message&mode=append&heading=%E2%9C%94%EF%B8%8F Todo"
