#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Journal
# @raycast.mode silent

# Optional parameters:
# @raycast.icon 🖊️
# @raycast.packageName jrnl.write
# @raycast.argument1 { "type": "text", "placeholder": "What are you doing?", "percentEncoded": true}

# Documentation:
# @raycast.description Log what your doing
# @raycast.author Niek
message=`echo $1 | python3 -c "import sys; from urllib.parse import unquote; print(unquote(sys.stdin.read()));"`
output=`/usr/local/bin/jrnl $message`
echo $message

