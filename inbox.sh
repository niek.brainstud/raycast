#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Inbox
# @raycast.mode silent

# Optional parameters:
# @raycast.icon 📨
# @raycast.packageName log.inbox

# Documentation:
# @raycast.description Write to your inbox
# @raycast.author Niek
open "obsidian://advanced-uri?vault=Notes&commandid=quickadd%253Achoice%253A6b2a05b7-9876-4b41-9bda-55d2e8c86c05"
