#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Note
# @raycast.mode silent

# Optional parameters:
# @raycast.icon 🖊️
# @raycast.packageName log.note

# Documentation:
# @raycast.description Write a note
# @raycast.author Niek
open "obsidian://advanced-uri?vault=Notes&commandid=periodic-notes%253Aopen-daily-note"
open "obsidian://advanced-uri?vault=Notes&commandid=quickadd%253Achoice%253A545d7a18-2e0f-4007-ac39-85fa4ef21465"
